/*
 * CSc103 Project 3: wordcount++
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;

unsigned long countWords(const string& s, set<string>& wl);

int main(int argc, const char *argv[])
{
	string line;
	set<string> words;
	set<string> lines;
	unsigned long nlines = 0, nwords = 0, nchars = 0;
	while(getline(cin,line)) {
		++nlines;
		nwords += countWords(line,words);
		nchars += line.length() + 1;  //add one for the newline.
		lines.insert(line);
	}
	cout << "\t" << nlines << "\t" << nwords << "\t" << nchars 
		 << "\t" << lines.size() << "\t" << words.size() << endl;
	return 0;
}

unsigned long countWords(const string& s, set<string>& wl)
{
	/* This function should return a count of the words in the
	 * string s, meanwhile adding each word to the set wl.
	 * NOTE: we'll just consider tab and space to be whitespace.
	 * */
	const int justReadWS = 0;
	const int justReadNonWS = 1;
	int currentState = justReadWS;
	string currentWord = "";
	unsigned long count = 0;
	for (size_t i = 0; i < s.length(); i++) {
		// process s[i] with our finite state machine.
		switch (currentState) {
			case justReadWS:
				if (s[i] != ' ' && s[i] != '\t') // reading non-whitespace
				{
					/* we should:
					 * increment word count.
					 * start tracking the current word.
					 * change the state. */
					count++;
					currentWord += s[i];
					currentState = justReadNonWS;
				}
				// else, there is nothing to do...
				break;
			case justReadNonWS:
				if (s[i] != ' ' && s[i] != '\t') // reading non-whitespace
					currentWord += s[i];
				else {
					currentState = justReadWS;
					wl.insert(currentWord);
					currentWord = "";
				}
				break;
			default:
				break;
				// if somehow our state was not one of
				// the two we listed... o_O
		}
	}
	// don't forget about the last word:
	if (currentWord != "")
		wl.insert(currentWord);

	return count;
}
