// arrays and pointers.
#include <iostream>
using std::cin;
using std::cout;
using std::endl;

/* TODO: write a function that returns void, and takes two
 * POINTERS to integers, and swaps the contents of those two
 * memory locations.  Note: part of this exercise is understanding
 * what I'm asking for...
 * */

/* TODO: write a function that performs a "circular shift" on
 * an array of integers.  For example, if the input array
 * contained 0,1,2,3,4 and we shifted by 2, the new array would
 * contain 3,4,0,1,2.
 * */

int main() {
	/* Arrays in C/C++: kind of like a "dumb" version of std::vector.
	 * It has no concept of its size.  It has no fancy member functions
	 * like push_back, etc... */
	/* Here's how to declare one: */
	int A[10];
	for (size_t i = 0; i < 10; i++) {
		A[i] = i*i;
	}
	/* what's in this array?  Well, consecutive squares.  But what
	 * really is in the symbol "A"? */
	cout << "A == " << A << "\n";
	/* Woah... just prints a memory address.  But that's all A is!!
	 * The address tells us where the array starts; i.e., the location
	 * of A[0].  The other elements are in adjacent addresses. */

	/* POINTERS...
	 * these are important.  They are just *variables that hold a
	 * memory address*.  E.g., our array A is a pointer.  What kinds of
	 * things might we want to do with pointers?  Well the obvious ones
	 * are:
	 * 1.  declare them
	 * 2.  initialize them
	 * 3.  read and write from the address stored in the pointer.
	 * */
	// 1. how to declare a pointer:
	int* p;  // note the *.  int* is a new datatype, called "pointer to int"
	// NOTE: we could also declare as int *p; which as we'll see, indicates
	// that *p is an integer.
	// 2.  how to initialize?  Well, you could just type in an address
	// p = 23452345; // usually this is a bad idea.  however...
	p = 0; // this is common.  The 0 address is never yours, so we can
		   // use it to distinguish good addresses from bad.  Often you
		   // will see "NULL" used as well.  This is a synonym for 0.
	int x = 22;
	// we can find out where x lives like this:
	p = &x;  // new stuff: usage of the ampersand.  If x had datatype blah,
	         // then &x is of type blah*

	cout << "p == " << p << "\tx == " << x << endl;
	// 3. how to read and write them??
	cout << "this is what's at the address stored in p: " << *p  << endl;
	// note the star operator.  AKA "dereference operator"
	(*p)++;
	cout << "p == " << p << "\tx == " << x << endl;
	// TODO: change the above statement to *p++ and see what is
	// printed out.
	// TODO: declare another pointer (say p2) to a character, initialize it
	// just as we did before, and then experiment with (*p2)++ vs.  *p2++
	// TODO: do the "self-test exercise" that I left in the notes online.
	// http://www-cs.ccny.cuny.edu/~wes/CSC103F12/docs/pointers-etc.pdf

	return 0;
}
