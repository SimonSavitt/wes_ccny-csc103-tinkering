// arrays and pointers, part 2.
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <vector>
using std::vector;

/* Example: write a function that returns void, and takes two
 * POINTERS to integers, and swaps the contents of those two
 * memory locations.
 * */
void swap (int* a, int* b) {
	int c= *a;
	*a=*b;
	*b=c;
}
// thanks to Maurice for the code :D

// NOTE: if you had two integers int x,y; to use this the call would be:
// swap(&x,&y);

/* Example: write a function that performs a "circular shift" on
 * an array of integers.  For example, if the input array
 * contained 0,1,2,3,4 and we shifted by 2, the new array would
 * contain 3,4,0,1,2.  NOTE: the array does not know its size,
 * so you have to send it as a parameter.  NOTE: since an array
 * is just a pointer, you don't have to pass it by reference.
 * */

void circularShift(int* A, int size, int shiftBy)
{
	shiftBy %= size; // does the same thing as shiftBy = shiftBy % size
	/* where does A[0] end up?  Should go to A[shiftBy].
	 * In general, A[i] --> A[(i+shiftBy)%size]
	 * However, the naive approach will not work:
	 * */
	for (int i = 0; i < size; i++) {
		A[(i+shiftBy) % size] = A[i];
	}
	// TODO: make this not broken.
	// NOTE: you can do this with extra storage (allocating a new array,
	// or using a vector) but if you have time, try to do it with only
	// a *constant* amount of additional storage (so the memory you
	// allocate here should be independent of size).  There are several
	// interesting solutions to this problem, some of which involve math  : )
	// remind me next time and I'll show you mine.
}


int main()
{
	int A[10];
	for (size_t i = 0; i < 10; i++) {
		A[i] = i*i;
	}

	/* let's try one more example from last time's TODO's: */
	int x = 22;
	int* p = &x;
	cout << "x=" << x << "\t&x=" << &x << "\tp=" << p << "\t*p=" << *p << endl;
	(*p)++;
	cout << "x=" << x << "\t&x=" << &x << "\tp=" << p << "\t*p=" << *p << endl;
	*p++;                                 
	cout << "x=" << x << "\t&x=" << &x << "\tp=" << p << "\t*p=" << *p << endl;
	/* Whoops.  The * operator has lower precedence than that of ++.  So, we
	 * just read from whatever happened to be next to x in memory.  No seg
	 * fault, since this is probably still owned by our process, but this is
	 * clearly not a great idea...
	 * */

	// NOTE: what does the bracket operator do?  ([])
	// A[i] is really just shorthand for the following: *(A+i)
	// bizarre consequences:
	for (unsigned long i = 0; i < 10; i++) {
		cout << A[i] << "\t" << *(A+i) << "\t" << i[A] << endl;
	}
	return 0;
}

/* Example: write a function that takes two vectors, and shuffles
 * them together (we've seen this before).  The new twist this
 * time, is that the vectors do not need to have the same size.
 * Thus, you may need to determine the longer of the two lists
 * to append the rest of its elements at the end.  There are a
 * lot of ways to do this; here's what I want you to do: try to
 * allocate two POINTERS named "shorter" and "longer" and have
 * them point to the shorter and longer vectors, respectively.
 * */

vector<int> shuffleVectors(vector<int>& v1, vector<int>& v2)
{
	vector<int>* shorter = &v2;
	vector<int>* longer  = &v1;
	if (v1.size() < v2.size()) {
		shorter = &v1;
		longer = &v2;
	}
	// now we can write our code just in terms of shorter and longer.
	/* NOTE: why is this better than just declaring two new vectors called
	 * shorter and longer?  Without the pointers, we'd have to make
	 * copies of the entire vector. */
	vector<int> result;
	for (size_t i = 0; i < (*shorter).size(); i++) {
		result.push_back((*shorter)[i]);
		result.push_back((*longer)[i]);
	}
	// NOTE: there is shorthand for the (*pointer).something construct.
	// we could have written the above loop as follows, using the -> syntax:
	// for (size_t i = 0; i < shorter->size(); i++) {...}

	// now we've used up the shorter list completely; just
	// append the rest of the longer one.
	for (size_t i = shorter->size(); i < longer->size(); i++) {
		result.push_back(longer->at(i));
	}
	return result;
}

// TODO: try to rewrite the above function by yourself.
