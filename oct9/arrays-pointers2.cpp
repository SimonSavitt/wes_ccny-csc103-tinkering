// arrays and pointers, part 2.
#include <iostream>
using std::cin;
using std::cout;
using std::endl;

/* Example: write a function that performs a "circular shift" on
 * an array of integers.  For example, if the input array
 * contained 0,1,2,3,4 and we shifted by 2, the new array would
 * contain 3,4,0,1,2.  NOTE: the array does not know its size,
 * so you have to send it as a parameter.  NOTE: since an array
 * is just a pointer, you don't have to pass it by reference.
 * */

int gcd(int a, int b)
{
	return (a%b)?gcd(b,a%b):b;
}

// shift A circularly to the right by s positions.
// n is the size of the array.
void circularShift(int* A, int n, int s)
{
	/* where does A[0] end up?  Should go to A[s].
	 * In general, A[i] --> A[(i+s)%n]
	 * However, the naive approach will not work.
	 * You could of course shift by one s times, but this
	 * will cost n*s statements!  Doesn't seem necessary.
	 * So how to do this with small extra storage and without
	 * a lot of extra statements???  Use math.
	 * */

	s %= n; // behavior of s mod n is all that matters.
	int d = gcd(n,s);
	for (int j = 0; j < d; j++) {
		// start at j and bounce around n/d times.
		int i = j;
		int tmp1,tmp0 = A[i];
		for (int k = 0; k < n/d; k++) {
			tmp1 = A[(i+s)%n];
			A[(i+s)%n] = tmp0;
			tmp0 = tmp1;
			i = (i+s) % n;
		}
	}
}

/* TODO: verify that my procedure uses less than c*n statements for some
 * constant c.
 * TODO: verify that my procedure only uses a constant amount of space,
 * assuming that the gcd algorithm uses only constant space.
 * NOTE: it is not obvious that the gcd would take only constant space as I
 * have written it; it is only due to a compiler optimization for "tail
 * recursion" that makes it so.  Nevertheless, we could have written a simple
 * non-recursive version that takes constant space without much effort.
 * */


int main()
{
	const size_t size = 21;
	int A[size];
	for (size_t i = 0; i < size; i++) {
		A[i] = i;
	}
	circularShift(A,size,18);
	for (size_t i = 0; i < size; i++) {
		cout << A[i] << "  ";
	}
	cout << endl;
	return 0;
}
