#include "rectangle.h"

int main(void)
{
	rectangle R(8,5);
	R.draw(); // NOTE: there's a hidden parameter in these function calls.
			  // the "this" pointer gives us access to whose member function
			  // is being called...
	return 0;
}
