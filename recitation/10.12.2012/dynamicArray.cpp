#include <iostream>
#include <new>

using namespace std;

int main()
{

int * arr;
int size ;
//dynamically allocates an array
cout<<"Enter the size\n";
cin >> size;

arr =  new int[size];

int i=0;

for(i=0;i<size;i++)
{
//assign value i to the location at (arr+i)
//*(arr+i) = i;
arr[i] = i;
}

for(i=0;i<size;i++)
{
//print the values
cout<< *(arr+i) <<endl;
}

//delete the memory we allocated so we don't get leaks.
delete[] arr;
cout<<"We deleted\n";
}