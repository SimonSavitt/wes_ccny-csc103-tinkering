#include <iostream>
#include <vector>

using namespace std;

int findMin( vector<int> & );

int main() {
	
	vector<int> vectorOne(20);
	
	/*  run through the vector and display each element, if possible */
   
	for (int index=0; index<20; ++index) {
        vectorOne[ index]=index;  //system does not check that vec is large enough to put values in here.
     // vectorOne.push_back(index); //this method of adding elements will cause the vec to automatically expand
	}
    
   
    
    vector<int>::iterator it = vectorOne.begin();
    vectorOne.insert(it+5, -99);
    
     for (int index=0; index<vectorOne.size(); ++index) {
        cout << vectorOne[index] <<"\n";
      //  cout<<vectorOne.at(index)<<"\n";
	}
    
    
    cout<< "Min is at "<< findMin(vectorOne)<<endl;
	
   for (int index=0; index<vectorOne.size(); ++index) {
        cout << vectorOne[index] <<"\n";
	}
    
    
	return 0;
}

//findMin returns the position of the smallest element of the vector
int findMin( vector<int> & v)
{
int i, sz, min, minIndex=0;
sz = v.size();

if(sz<1)
    return -1;
    
min = v[0];

for(i=0;i<sz;i++)
{
if(v[i]<min)
    {
    min = v[i];
    minIndex = i;
    }
}
return minIndex;
}

