#include <iostream>
#include <fstream>
#include <set>
#include <string>

using std::string;
using std::cout;
using std::set;
using std::ifstream;

set<string> dictionary;  //Declares a set of strings called dictionary with global scope

/*
removeSub takes a sInput, a string, as a reference parameter.
It also takes sub, another string.  If the string sub
occurs as a substring in sInput, the first occurence is removed.
This effect will is persistent to the caller since sInput is passed by reference
*/
void removeSub(string& sInput, const string& sub);

int main()
{

ifstream fin; //declares a file input stream object
fin.open("dictionary.txt");  
/* we call the open method of that file input stream
   open takes a string parameter, the name of the file we want to open
   Note that we can just write the name of the file if it will always be
   present in the CURRENT WORKING DIRECTORY that our program is running in.
   But sometimes if the file is in another folder, we might pass in the entire path,
   i.e.  fin.open("/home/csc103/workspace/csc103-tinkering/dictionary.txt");
*/

string tmp;  //we declare a string to store words temporarily as we read from file

/* The condition for the while loop below will 
   fail when the input stream operator >> can't read
   a string from fin, which will break the loop when
   we reach end of file.
*/
while(fin >> tmp)
{
dictionary.insert(tmp);  //we insert each word we read in the dictionary
}

cout<<"Dictionary is read\n";

//the loop just prints the contents of the dictionary
//uncomment the block if you want to see the contents of
//dictionary printed to standard output 
/*
for (set<string>::iterator i = dictionary.begin(); i != dictionary.end(); i++) {
		cout << *i << "\n";
	}
*/
string s1 = "The red quick red fox";
string s2 = "red";
cout<<s1<<"\n";  //Outputs "The red quick red fox"
 removeSub(s1, s2);  //removes the first occurence of "red" from s1
cout<< s1 <<"\n"; //Outputs "The  quick red fox"
}


void removeSub(string& sInput, const string& sub)
{
	int foundpos = sInput.find(sub); //use string library function find to see if and where sub occurs in sInput
	if ( foundpos != -1 )  //find returns -1 if the substring is not found.  In which case, we don't need to remove anything from sInput
		sInput.erase(sInput.begin() + foundpos, sInput.begin() + foundpos + sub.length());
        //then we use the erase function to remove the characters from the start of the substring to the end.
}