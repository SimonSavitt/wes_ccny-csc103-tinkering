// Introducing vectors; more on functions.

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <vector>
using std::vector;

int main()
{
	/* problem: gather a list of strings from 
	 * standard input, and print them in reverse order.
	 * Read strings until standard input runs out, or the
	 * string is "__DONE__". */
	/* Naive attempt:
		string s1;
		string s2;
		string s3;...
	*/
	// this doesn't work...  we don't know how many strings
	// we need until the program is actually running :/
	// This is a job for the *vector*.
	// NOTE:  Important!  vector is NOT A DATATYPE.
	// The vector is a *machine for making datatypes*.
	//vector v; // will not compile.  TODO: try it.
	// Vector will take in an existing datatype, and then
	// gives you a *indexed collection* of that datatype.
	// Here is the syntax:
	vector<string> V;
	// to add a string to V, use the "push_back(...)" function
	// e.g. string s; cin >> s; V.push_back(s);
	// How to read the contents??  Use the square brackets:
	// cout << V[0];  // this is the first thing in the vector.
	// How to figure out the size of the vector??  Like this:
	// cout << V.size();

	// Now let's finally attempt to solve the problem:
	string s;
	while(cin >> s) {
		// store s in the vector
		if (s == "__DONE__")
			break;
		V.push_back(s);
	}
	// now print them in reverse:
	for (int i=V.size()-1; i>=0; i--)
		cout << V[i] << "\n";

	// exercise:  gather two lists of strings from input
	// (say of length 5) and then echo the shuffled list
	// to standard output
	// Code thanks to Raphael:

	vector<string> lista;
	vector<string> listb;

	for(int i= 0;i <5; i++ ){
		cin >> s;
		lista.push_back(s);
	}

	for(int i= 0;i <5; i++ ){
		cin >> s;
		listb.push_back(s);
	}

	 for (size_t i = 0; i <5 ; i++) {
	 	cout << lista[i] << "\n";
	 	cout << listb[i] << "\n";
	 }


	/* TODO: write a *function* that takes two vectors of
	 * strings, and returns a vector with the strings shuffled.
	 * You can assume that both vectors have the same length.
	 * Remember: you have to write it OUTSIDE of main()... */

	/* TODO: make the function work in the case where they don't
	 * necessarily have the same length. */

	 /* TODO: write some code to test the function you just wrote. */
	return 0;
}
